#![no_std]

pub use hirun_macros::future;

pub(crate) mod platform;

pub mod channel;
pub mod event;
pub mod runtime;
pub mod thread;
pub mod time;
pub mod net;

pub use hierr::{Error, prelude};
