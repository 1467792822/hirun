use super::{mut_buf, const_buf};
use crate::Error;

pub struct Fd {
    pub(crate) fd: i32,
}

impl Fd {
    pub fn new(fd: i32) -> Self {
        Self { fd }
    }

    pub fn fd(&self) -> i32 {
        self.fd
    }

    pub fn close(&mut self) {
        unsafe { libc::close(self.fd) };
        self.fd = -1;
    }

    pub fn try_read(&self, buf: &mut [u8]) -> Result<usize, Error> {
        loop {
            let ret = unsafe { libc::read(self.fd, mut_buf(buf), buf.len()) };
            if ret >= 0 {
                return Ok(ret as usize);
            } else {
                let err = Error::last_error();
                if err.errno != libc::EINTR {
                    return Err(err);
                }
            }
        }
    }

    pub fn try_write(&self, buf: &[u8]) -> Result<usize, Error> {
        loop {
            let ret = unsafe { libc::write(self.fd, const_buf(buf), buf.len()) };
            if ret >= 0 {
                return Ok(ret as usize);
            } else {
                let err = Error::last_error();
                if err.errno != libc::EINTR {
                    return Err(err);
                }
            }
        }
    }

    pub fn try_sendfile(&self, in_fd: i32, off: usize, count: usize) -> Result<usize, Error> {
        let mut off = off as i64;
        loop {
            let ret = unsafe { libc::sendfile(self.fd, in_fd, &mut off, count) };
            if ret >= 0 {
                return Ok(ret as usize);
            } else {
                let err = Error::last_error();
                if err.errno != libc::EINTR {
                    return Err(err);
                }
            }
        }
    }
}

impl Clone for Fd {
    fn clone(&self) -> Self {
        Self {
            fd: unsafe { libc::dup(self.fd) },
        }
    }
}

impl Drop for Fd {
    fn drop(&mut self) {
        unsafe { libc::close(self.fd) };
    }
}

