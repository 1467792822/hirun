#[cfg_attr(target_family = "unix", link(name = "c"))]
#[cfg_attr(target_family = "windows", link(name = "Ws2_32"))]
extern "C" {
    pub(crate) fn htons(val: u16) -> u16;
    pub(crate) fn ntohs(val: u16) -> u16;
    pub(crate) fn inet_pton(family: i32, name: *const u8, addr: *mut u8) -> i32;
    pub(crate) fn inet_ntop(family: i32, name: *const u8, addr: *mut u8, len: u32) -> *const u8;
}

// C11
#[allow(dead_code)]
#[link(name = "c")]
extern "C" {
    pub(crate) fn timespec_get(tm: *mut libc::timespec, base: i32) -> i32;
}
