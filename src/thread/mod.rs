
cfg_if::cfg_if! {
    if #[cfg(unix)] {
        mod pthread;
        pub use pthread::*;
    } else {
        mod c11;
        pub use c11::*;
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn test_spawn() {
        const VAL: i32 = 100;
        let val_ref = &VAL;
        let val = super::spawn(|| *val_ref + 100).join().unwrap();
        assert_eq!(val, 200);
    }
}
