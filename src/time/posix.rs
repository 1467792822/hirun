
use core::ptr;
use core::time::Duration;

pub fn now() -> Duration {
    let mut now = libc::timespec {
        tv_sec: 0,
        tv_nsec: 0,
    };

    let _ = unsafe { libc::clock_gettime(libc::CLOCK_MONOTONIC, ptr::addr_of_mut!(now)) };
    Duration::new(now.tv_sec as u64, now.tv_nsec as u32)
}
