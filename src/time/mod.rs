
cfg_if::cfg_if! {
    if #[cfg(target_family = "unix")] {
        mod posix;
        pub use posix::*;
    } else {
        mod c11;
        pub use c11::*;
    }
}
